# Xfigart #
Clip art for use with [Xfig](https://sourceforge.net/projects/mcj/), a diagramming tool.
## Prerequisites ##
You will, of course, need to have [Xfig and Fig2dev](http://mcj.sourceforge.net/installation.html) installed.
### Xfig on Microsoft Windows ###
You can run Xfig on Windows via [Cygwin](https://www.cygwin.com/). I've had good luck with the [VcXsrv](https://sourceforge.net/projects/vcxsrv/) X Server handling high resolution displays. I have [some instructions](https://www.eventuallabs.com/blog/fix_fonts_xfig_cygwin/index.html) for installing some [Ghostscript fonts](https://sourceforge.net/projects/gs-fonts/files/gs-fonts/8.11%20%28base%2035%2C%20GPL%29/) needed for zooming in and out.
## Using this library ##
Starting Xfig with
```
xfig -library_dir <path to xfigart>/fig
```
will give Xfig access to these figures. For example,
```
xfig -library_dir xfigart/fig
```
...will work if you're in the same directory as Xfigart. If you also want access to the standard Xfig clip art, you can navigate to `xfigart/fig` and
```
ln -s /usr/share/xfig/Libraries/ xfiglib
```
...to make a symbolic link to the figs that ship with Xfig. Your libraries may not be in `/usr/share/xfig`.
You can also just use *File* &rarr; *Merge* and navigate to one of the fig files to merge that file with whatever you're working on.
## Library contents ##
### cables/metal_disc_electrode.fig ###

 ![metal_disc_electrode](600x/cables/metal_disc_electrode_blog.png) 


### cables/ribbon_plug_10pin_100mil.fig ###

 ![ribbon_plug_10pin_100mil](600x/cables/ribbon_plug_10pin_100mil_blog.png) 


### cables/3.5mm_right_angle_plug.fig ###

 ![3.5mm_right_angle_plug](600x/cables/3.5mm_right_angle_plug_blog.png) 


### cables/2pos_mini_fit_jr_plug.fig ###

 ![2pos_mini_fit_jr_plug](600x/cables/2pos_mini_fit_jr_plug_blog.png) 


### cables/alligator_clip.fig ###

 ![alligator_clip](600x/cables/alligator_clip_blog.png) 


### cables/ribbon_plug_12pin_100mil.fig ###

 ![ribbon_plug_12pin_100mil](600x/cables/ribbon_plug_12pin_100mil_blog.png) 


### cables/bnc_male_plug.fig ###

 ![bnc_male_plug](600x/cables/bnc_male_plug_blog.png) 


### cables/oscilloscope_probe.fig ###

 ![oscilloscope_probe](600x/cables/oscilloscope_probe_blog.png) 


### cables/liquid_junction_electrode.fig ###

 ![liquid_junction_electrode](600x/cables/liquid_junction_electrode_blog.png) 


### titleblock/titleblock_letter_template.fig ###

 ![titleblock_letter_template](600x/titleblock/titleblock_letter_template_blog.png) 


### devboards/pololu_usb_avr_v2_1.fig ###

 ![pololu_usb_avr_v2_1](600x/devboards/pololu_usb_avr_v2_1_blog.png) 


### devboards/nano_wiring.fig ###

 ![nano_wiring](600x/devboards/nano_wiring_blog.png) 


### devboards/arduino_nano.fig ###

 ![arduino_nano](600x/devboards/arduino_nano_blog.png) 


### speakers/ces-571423-28pm_side.fig ###

 ![ces-571423-28pm_side](600x/speakers/ces-571423-28pm_side_blog.png) 


### speakers/ces-571423-28pm_top.fig ###

 ![ces-571423-28pm_top](600x/speakers/ces-571423-28pm_top_blog.png) 


