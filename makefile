project_name = xfigart

# Directory for xfig files.  Make will automatically convert all fig
# files in this directory and its subdirectories into png files
fig_directory = fig


###################### Done with configuration #######################

# Add all subdirectories for xfig files
fig_directories = $(sort $(dir $(wildcard $(fig_directory)/*/)))

fig_files = $(wildcard $(addsuffix *.fig, $(fig_directories)))
eps_files = $(subst fig,eps,$(addsuffix .eps, $(basename $(fig_files))))
png_files = $(subst fig,png,$(addsuffix .png, $(basename $(fig_files))))
600x_png_files = $(addsuffix _blog.png, $(subst fig,600x,$(basename $(fig_files))))



usage_text_width = 20
indent_text_width = 7
target_text_width = 15

# All these printf statements look confusing, but the idea here is to
# separate the layout from the content.  Always use the same printf
# line with a continuation character at the end.  Put your content on
# the next line.
help:
	@echo "Makefile for $(project_name)"
	@echo ""
	@echo "Usage:"
	@printf "%$(indent_text_width)s %-$(target_text_width)s %s\n" \
          "make" "pngs" \
          "Create pngs from xfig files"
	@printf "%$(indent_text_width)s %-$(target_text_width)s %s\n" \
          "make" "readme" \
          "Update README file with contents of 600x directory"
	@printf "%$(indent_text_width)s %-$(target_text_width)s %s\n" \
          "make" "clean" \
          "Clean up temporary files"


debug:
	@echo "fig directories: $(fig_directories)"
	@echo "fig files: $(fig_files)"
	@echo "eps files: $(eps_files)"
	@echo "png files: $(png_files)"
	@echo ""
	@echo "600x png files: $(600x_png_files)"

pngs: $(eps_files) $(png_files) $(600x_png_files)

.PHONY: readme
readme: pngs
	tclsh figlist.tcl


# Convert to png using ghostscript.
png/%.png: eps/%.eps
	mkdir -p $(dir $@)
	gs -dSAFER -dBATCH -dNOPAUSE -sDEVICE=png16m -dGraphicsAlphaBits=4 \
          -dTextAlphaBits=4 -dEPSCrop -sOutputFile="$@" -r300 "$<"


eps/%.eps: fig/%.fig
	mkdir -p $(dir $@)
	fig2dev -L eps -m 1 $< $@

600x/%_blog.png: png/%.png
	mkdir -p $(dir $@)
	convert -resize "600x400>" $< $@


