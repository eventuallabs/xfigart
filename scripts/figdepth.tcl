######################## Global configuration ########################

# The name of this program.  This will get used to identify logfiles,
# configuration files and other file outputs.
set program_name figdepth

set thisfile [file normalize [info script]]

# Directory where this script lives
set program_directory [file dirname $thisfile]

# Directory from which the script was invoked
set invoked_directory [pwd]

# This software's version.  Anything set here will be clobbered by the
# makefile when starpacks are built.
set revcode 1.0

set root_directory [file dirname $argv0]

######################## Command line parsing ########################

package require cmdline
set usage "usage: $program_name \[options\] file"

set options {
    {d.arg "50" "Minimum depth of output drawing"}
    {o.arg "figdepth" "Output file name base"}

}

try {
    array set params [::cmdline::getoptions argv $options $usage]
} trap {CMDLINE USAGE} {message optdict} {
    # Trap the usage signal, print the message, and exit the application.
    # Note: Other errors are not caught and passed through to higher levels!
    puts $message
    exit 1
}

# After cmdline is done, argv will point to the last argument
set input_file_name_list [list]
foreach filename $argv {
    lappend input_file_name_list $filename
}


# Provides splitx needed to split strings on substrings
package require textutil

proc iterint {start points} {
    # Return a list of increasing integers starting with start with
    # length points
    set count 0
    set intlist [list]
    while {$count < $points} {
	lappend intlist [expr $start + $count]
	incr count
    }
    return $intlist
}

proc read_file {filename} {
    # Return a list of lines from an input file.  Strips out header.
    #
    # Arguments:
    #   filename -- relative or full path to file
    try {
	set fid [open $filename r]
    } trap {} {message optdict} {
	puts $message
	exit
    }
    set raw_datafile_list [split [read $fid] "\n"]
    foreach line $raw_datafile_list {	
	set line [string trim $line]
	if {[string length $line] == 0} {
	    # This line is empty
	    continue
	}
	lappend datafile_list $line
    }
    return $datafile_list
}

proc get_object_depth { xfig_line } {
    # Return the depth of the object
    #
    # Arguments:
    #   xfig_line -- Single line from an xfig file

    # See
    # http://mcj.sourceforge.net/fig-format.html
    # ...for the fig format specification
    set entry_list [split $xfig_line]
    set first_character [lindex $entry_list 0]
    switch $first_character {
	"0" {
	    # Color object
	    # puts "Found a color object"
	    set old_depth ""
	}
	"1" {
	    # Ellipse
	    set depth_index 6
	    set old_depth [lindex $entry_list $depth_index]
	    # puts "Found an ellipse at depth $old_depth"
	}
	"2" {
	    # Polyline (also imported picture bounding boxes)
	    set depth_index 6
	    set old_depth [lindex $entry_list $depth_index]
	    # puts "Found a polyline at depth $old_depth"
	}
	"3" {
	    # Spline
	    set depth_index 6
	    set old_depth [lindex $entry_list $depth_index]
	    # puts "Found a spline at depth $old_depth"
	}
	"4" {
	    # Text
	    set depth_index 3
	    set old_depth [lindex $entry_list $depth_index]
	    # puts "Found some text at depth $old_depth"
	}
	"5" {
	    # Arc
	    set depth_index 6
	    set old_depth [lindex $entry_list $depth_index]
	    # puts "Found an arc at depth $old_depth"
	}
	"6" {
	    # Compound -- no depth
	    # puts "Found a compound object"
	    set old_depth ""
	}
	default {
	    set old_depth ""
	}
    }
    return $old_depth
}

proc increase_object_depth { xfig_line depth_increase} {
    # Return a new xfig line with the depth increased
    #
    # Arguments:
    #   xfig_line -- Single line from an xfig file
    #   depth_increase -- Amount to increase the depth
    set entry_list [split $xfig_line]
    set first_character [lindex $entry_list 0]
    switch $first_character {
	"0" {
	    # Color object -- nothing to do
	    return $xfig_line
	}
	"1" {
	    # Ellipse
	    set depth_index 6
	    set old_depth [get_object_depth $xfig_line]
	    set new_depth [expr $old_depth + $depth_increase]
	    set new_entry_list [lset entry_list $depth_index $new_depth]
	    set new_line [join $new_entry_list]
	    return $new_line
	}
	"2" {
	    # Polyline (also imported picture bounding boxes)
	    set depth_index 6
	    set old_depth [get_object_depth $xfig_line]
	    set new_depth [expr $old_depth + $depth_increase]
	    set new_entry_list [lset entry_list $depth_index $new_depth]
	    set new_line [join $new_entry_list]
	    return $new_line
	}
	"3" {
	    # Spline
	    set depth_index 6
	    set old_depth [get_object_depth $xfig_line]
	    set new_depth [expr $old_depth + $depth_increase]
	    set new_entry_list [lset entry_list $depth_index $new_depth]
	    set new_line [join $new_entry_list]
	    return $new_line
	}
	"4" {
	    # Text
	    set depth_index 3
	    set old_depth [get_object_depth $xfig_line]
	    set new_depth [expr $old_depth + $depth_increase]
	    set new_entry_list [lset entry_list $depth_index $new_depth]
	    set new_line [join $new_entry_list]
	    return $new_line
	}
	"5" {
	    # Arc
	    set depth_index 6
	    set old_depth [get_object_depth $xfig_line]
	    set new_depth [expr $old_depth + $depth_increase]
	    set new_entry_list [lset entry_list $depth_index $new_depth]
	    set new_line [join $new_entry_list]
	    return $new_line
	}
	"6" {
	    # Compound -- nothing to do
	    return $xfig_line
	}
	default {
	    # Some non-object -- nothing to do
	    return $xfig_line
	}
    }
}



########################## Main entry point ##########################

set input_file [lindex $input_file_name_list 0]
set input_line_list [read_file $input_file]

# Set the initial minimum depth super high
set minimum_depth 1000
foreach line $input_line_list {
    set depth [get_object_depth $line]
    if { $depth eq "" } {
	continue
    }
    if { $depth < $minimum_depth } {
	set minimum_depth $depth
    }
}
set depth_increase [expr $params(d) - $minimum_depth]
if { $depth_increase > 0 } {
    puts "Minimum depth is $minimum_depth.  Increasing depth of all objects by $depth_increase."
    foreach line $input_line_list {
	
	lappend output_line_list [increase_object_depth $line $depth_increase]
    }
}

# Write the new fig file
set output_file_tail $params(o).fig
set output_file_path ${invoked_directory}/$output_file_tail
try {
    set fid [open $output_file_path w]
} trap {} {message optdict} {
    puts $message
    exit
}

puts ""
puts "Writing $output_file_path"
foreach line $output_line_list {
    puts $fid "$line"
}
close $fid
